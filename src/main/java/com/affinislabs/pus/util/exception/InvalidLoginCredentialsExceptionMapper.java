/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.util.exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author Lateefah
 */
@Provider
public class InvalidLoginCredentialsExceptionMapper implements ExceptionMapper<InvalidLoginCredentialsException> {  

    @Override
    public Response toResponse(InvalidLoginCredentialsException ilce) {
        return Response.status(ilce.getStatus())
                .entity(new ErrorMessage(ilce))
                .type(MediaType.APPLICATION_JSON).
                build();
    }
    
}