/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.util.exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author Lateefah
 */
@Provider
public class InvalidEmailFormatExceptionMapper implements ExceptionMapper<InvalidEmailFormatException> {  

    @Override
    public Response toResponse(InvalidEmailFormatException iefe) {
        return Response.status(iefe.getStatus())
                .entity(new ErrorMessage(iefe))
                .type(MediaType.APPLICATION_JSON).
                build();
    }
    
}
