/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.util.exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author buls
 */
@Provider
public class InvalidUserTypeExceptionMapper implements ExceptionMapper<InvalidUserTypeException> {  

    @Override
    public Response toResponse(InvalidUserTypeException iute) {
        return Response.status(iute.getStatus())
                .entity(new ErrorMessage(iute))
                .type(MediaType.APPLICATION_JSON).
                build();
    }
    
}
