/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author buls
 */

@Entity
@Table(name = "password_reset_token")
public class UserPasswordResetToken implements Serializable {
    @Id
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "email")
    private String email;
    
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "token")
    private String token;
    
    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "expiry_time")
    private Date expiryTime;
    
    @Transient
    private String firstName;
    
    public UserPasswordResetToken() {
        
    }
    
    public UserPasswordResetToken(String email, String token, String firstName) {
        this.email = email;
        this.token = token;
        this.expiryTime = Calendar.getInstance().getTime();
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(Date expiryTime) {
        this.expiryTime = expiryTime;
    }    

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }        
    
}
