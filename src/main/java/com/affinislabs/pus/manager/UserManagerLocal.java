/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.manager;

import com.affinislabs.pus.model.User;
import com.affinislabs.pus.model.UserRole;
import com.affinislabs.pus.pojo.AppRole;
import com.affinislabs.pus.pojo.AppUser;
import com.affinislabs.pus.pojo.PlatformCallback;
import com.affinislabs.pus.util.exception.DuplicateUserKeyException;
import com.affinislabs.pus.util.exception.InternalException;
import com.affinislabs.pus.util.exception.InvalidEmailFormatException;
import com.affinislabs.pus.util.exception.InvalidLoginCredentialsException;
import com.affinislabs.pus.util.exception.InvalidUserTypeException;
import com.affinislabs.pus.util.exception.NullUserAttributeException;
import com.affinislabs.pus.util.exception.UserDoesNotExistException;

/**
 *
 * @author Lateefah
 */
public interface UserManagerLocal {
 
    AppUser getAppUser (String userId);
    
    AppUser getAppUser (User user);
    
    AppRole getAppRole (UserRole userRole);
    
    AppUser authenticateUser (String username, String password)
            throws NullUserAttributeException, InvalidEmailFormatException, InvalidLoginCredentialsException;
    
    AppUser getUserDetails (String email) 
            throws NullUserAttributeException, InvalidEmailFormatException, UserDoesNotExistException; 
    
    AppUser addUser(String email, String password, String userType) 
            throws NullUserAttributeException, InvalidEmailFormatException, InvalidUserTypeException,
            DuplicateUserKeyException;
    
    Boolean addUserRole(String email, String roleId);
    
    PlatformCallback verifyEmail(String encodedEmail) 
            throws NullUserAttributeException, UserDoesNotExistException, InternalException;
    
    AppUser verifySmsCode(String email, String smsCode) 
            throws NullUserAttributeException, UserDoesNotExistException;
    
    AppUser resendSmsVerification(String email)
            throws NullUserAttributeException, UserDoesNotExistException;
    
    AppUser resendEmailVerification(String email)
            throws NullUserAttributeException, UserDoesNotExistException;
    
    AppUser resetPassword(String email)
             throws NullUserAttributeException, InvalidEmailFormatException, UserDoesNotExistException;
    
    AppUser setNewPassword (String email, String password)
            throws NullUserAttributeException, InvalidEmailFormatException;
    
    String authenticatePasswordResetToken(String email, String token)
            throws NullUserAttributeException, UserDoesNotExistException;
    
    boolean removeDonCasino();
    
    boolean removeSadiq();
    
    boolean removeLattie();    
    
}