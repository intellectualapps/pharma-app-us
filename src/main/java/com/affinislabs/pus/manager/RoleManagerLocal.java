/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.manager;

import java.util.List;
import com.affinislabs.pus.model.Role;
import com.affinislabs.pus.model.RolePrivilege;

/**
 *
 * @author buls
 */
public interface RoleManagerLocal {
    
    Role getRole (String roleId);
    
    List<Role> getAllRoles();
    
    Role addRole(String roleId, String description);
    
    Boolean deleteRole(String roleId);
    
    Role updateRole(String roleId, String description);
    
    List<RolePrivilege> getRolePrivileges(String roleId);
    
    Boolean addRolePrivilege(String roleId, String privilegeId);
    
    Boolean deleteRolePrivilege(String roleId, String privilegeId);
    
    
}
