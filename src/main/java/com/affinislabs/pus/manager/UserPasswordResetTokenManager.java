/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.manager;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.affinislabs.pus.data.manager.UserPasswordResetTokenDataManagerLocal;
import com.affinislabs.pus.model.UserPasswordResetToken;
import com.affinislabs.pus.util.CodeGenerator;

/**
 *
 * @author buls
 */
@Stateless
public class UserPasswordResetTokenManager implements UserPasswordResetTokenManagerLocal {

    @EJB
    private UserPasswordResetTokenDataManagerLocal userPasswordResetTokenDataManager;
    @EJB
    private CodeGenerator codeGenerator;

    @Override
    public UserPasswordResetToken create(String email, String firstName) {
        UserPasswordResetToken existingToken = userPasswordResetTokenDataManager.get(email);
        UserPasswordResetToken newToken = new UserPasswordResetToken(email, codeGenerator.getToken(), firstName);
        if (existingToken != null) {
            userPasswordResetTokenDataManager.delete(existingToken);            
            return userPasswordResetTokenDataManager.create(newToken);
        } else {
            return userPasswordResetTokenDataManager.create(newToken);
        }
    }

    @Override
    public void delete(String email) {
        UserPasswordResetToken userToken = userPasswordResetTokenDataManager.get(email);
        if (userToken != null) {
            userPasswordResetTokenDataManager.delete(userToken);
        }
    }

    @Override
    public UserPasswordResetToken get(String email) {
        return userPasswordResetTokenDataManager.get(email);
    }

}
