/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.manager;

import com.affinislabs.pus.model.UserPasswordResetToken;

/**
 *
 * @author buls
 */
public interface UserPasswordResetTokenManagerLocal {

    UserPasswordResetToken create(String email, String firstName);
    
    void delete(String email);
    
    UserPasswordResetToken get(String email);
}