/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.manager;

import java.util.List;
import com.affinislabs.pus.model.Privilege;

/**
 *
 * @author buls
 */
public interface PrivilegeManagerLocal {
    
    Privilege getPrivilege (String privilegeId);
    
    List<Privilege> getAllPrivileges ();
    
    Privilege getAppPrivilege(Privilege privilege);
    
    Privilege addPrivilege(String privilegeId, String description, String menuLink);
    
    Boolean deletePrivilege(String privilegeId);
    
    Privilege updatePrivilege(String privilegeId, String description, String menuLink, String menuIcon);
}
