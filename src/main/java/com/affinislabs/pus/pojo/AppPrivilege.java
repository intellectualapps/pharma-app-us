/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.pojo;

import java.io.Serializable;

/**
 *
 * @author Lateefah
 */
public class AppPrivilege implements Serializable {
        
    private String privilegeId;
    private String description;

    public AppPrivilege() {
    }

    public AppPrivilege(String privilegeId) {
        this.privilegeId = privilegeId;
    }

    public AppPrivilege(String privilegeId, String description) {
        this.privilegeId = privilegeId;
        this.description = description;
    }

    public String getPrivilegeId() {
        return privilegeId;
    }

    public void setPrivilegeId(String privilegeId) {
        this.privilegeId = privilegeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
