/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affinislabs.pus.pojo;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Lateefah
 */
public class AppUser implements Serializable {
    private String email;    
    private String firstName;    
    private String lastName; 
    private String phoneNumber; 
    private String password;
    private Date creationDate;
    private String smsVerificationCode;
    private String authToken;
    private boolean isEmailAddressVerified;
    private List<AppRole> userRoles;
    
    public AppUser() {
        userRoles = new ArrayList<AppRole>();
    }
    
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber () {
        return phoneNumber;
    }
    
    public void setPhoneNumber (String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public Date getCreationDate() {
        return creationDate;
    }
    
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getSmsVerificationCode() {
        return smsVerificationCode;
    }

    public void setSmsVerificationCode(String smsVerificationCode) {
        this.smsVerificationCode = smsVerificationCode;
    }

    public boolean getIsEmailAddressVerified() {
        return isEmailAddressVerified;
    }

    public void setIsEmailAddressVerified(boolean isEmailAddressVerified) {
        this.isEmailAddressVerified = isEmailAddressVerified;
    }
    
    public List<AppRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<AppRole> userRoles) {
        this.userRoles = userRoles;
    }
    
    public String getUserRolesAsJson(){
        ObjectMapper jsonMapper = new ObjectMapper();
        String userRolesInString = "";
        try {
            userRolesInString = jsonMapper.writeValueAsString(getUserRoles());
        } catch (Exception e){
            
        }
        
        return userRolesInString;
    }
    
    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
    
    public String getAuthToken() {
        return authToken;
    }
        
}
