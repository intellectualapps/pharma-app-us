 /*
 * Copyright 2016 Intellectual Apps Ltd.
 */
package com.affinislabs.pus.data.manager;


 import javax.ejb.Local;
 import java.util.List;
import com.affinislabs.pus.model.Privilege;

/**
 *
 * @author buls
 */
@Local
public interface PrivilegeDataManagerLocal {
    
    Privilege create(Privilege privilege);

    Privilege update(Privilege privilege);

    Privilege get(String privilegeId);

    void delete(Privilege privilege);

    List<Privilege> getAllPrivileges();

}
