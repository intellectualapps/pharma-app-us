 /*
 * Copyright Intellectual Apps Ltd.
 */
package com.affinislabs.pus.data.manager;

import java.util.HashMap;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;
import com.affinislabs.pus.data.provider.DataProviderLocal;
import com.affinislabs.pus.model.UserRole;

/**
 *
 * @author buls
 */
@Stateless
public class UserRoleDataManager implements UserRoleDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public UserRole create(UserRole userRole) {
        return crud.create(userRole);
    }

    @Override
    public UserRole update(UserRole userRole) {
        return crud.update(userRole);
    }

    @Override
    public UserRole get(String userId) {
        return crud.find(userId, UserRole.class);
    }

    @Override
    public void delete(UserRole userRole) {
        crud.delete(userRole);
    }

    @Override
    public List<UserRole> getByUser(String email) {
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("email", email);
        List<UserRole> userRoles = crud
            .findByNamedQuery("UserRole.findByEmail",
                parameters,
                UserRole.class);

        return userRoles;
    }

    @Override
    public List<UserRole> getUsernameAndRoleId(String username, String roleId){
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("username", username);
        parameters.put("roleId", roleId);
        List<UserRole> userRoles = crud
            .findByNamedQuery("UserRole.findByEmailAndRoleId",
                parameters,
                UserRole.class);

        return userRoles;
    }
    
}
