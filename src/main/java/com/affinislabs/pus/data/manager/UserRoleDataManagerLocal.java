 /*
 * Copyright 2016 Intellectual Apps Ltd.
 */
package com.affinislabs.pus.data.manager;


 import javax.ejb.Local;
 import java.util.List;
import com.affinislabs.pus.model.UserRole;

/**
 *
 * @author buls
 */
@Local
public interface UserRoleDataManagerLocal {
    
    UserRole create(UserRole userRole);

    UserRole update(UserRole userRole);

    UserRole get(String userId);

    void delete(UserRole userRole);

    List<UserRole> getByUser(String userId); 
    
    List<UserRole> getUsernameAndRoleId(String username, String roleId);

}
